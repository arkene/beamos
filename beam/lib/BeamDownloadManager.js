var BeamDownloadManager = {	

	readLocalFile: function(filename, callback) {
		BeamPersistentStorage.root.getFile(filename, null, function(fileEntry) { 
	    fileEntry.file(function(file) { 
	      callback(file);
	    }, this.errorHandler); 
	  }, this.errorHandler); 
	},	

	
	
	saveFileLocally: function(data, mimetype, filename, callback) {
		BeamPersistentStorage.root.getFile(filename, {create : true}, 
			function(fileEntry) {
				// FileWriter				
				BEAM.log('type::');
				if($.type(data) != 'object'){
					data = new Blob([data],{type:'text/html'});
				}
				
				fileEntry.createWriter(function(writer) {   
				    writer.onprogress = function() { BEAM.log("Writing to file...") };  
				    writer.onwriteend = function(e) {
				    	BEAM.log("Write completed.");
				      	callback(mimetype, data.size, data);
				  	};  
				    writer.onerror = function(e) { };
				    writer.write(data); 
			    }, this.errorHandler); 
			}
		); 
	},
	
	errorHandler: function(e) {
		  var msg = '';
		  switch (e.code) {
		    case FileError.QUOTA_EXCEEDED_ERR:
		      msg = 'QUOTA_EXCEEDED_ERR';
		      break;
		    case FileError.NOT_FOUND_ERR:
		      msg = 'NOT_FOUND_ERR';
		      break;
		    case FileError.SECURITY_ERR:
		      msg = 'SECURITY_ERR';
		      break;
		    case FileError.INVALID_MODIFICATION_ERR:
		      msg = 'INVALID_MODIFICATION_ERR';
		      break;
		    case FileError.INVALID_STATE_ERR:
		      msg = 'INVALID_STATE_ERR';
		      break;
		    default:
		      msg = 'Unknown Error';
		      break;
		  };
		  BEAM.error(msg);
	}
};

